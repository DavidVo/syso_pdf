package org.pdf;

import java.io.FileNotFoundException;

import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Paragraph;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ) throws FileNotFoundException
    {
    	//OutputStream dest = ;
    	//Initialize writer
    	PdfWriter writer = new PdfWriter("C:\\Eclipse\\syso_pdf\\pdf\\target\\test3.pdf");

    	//Initialize document
    	PdfDocument pdfDoc = new PdfDocument(writer);
    	Document doc = new Document(pdfDoc);

    	//Add paragraph to the document
    	doc.add(new Paragraph("Hello World!"));

    	//Close document
    	doc.close();
    }
}
